package Negocio;

import java.util.ArrayList;

public class MatrizPesos {
	
	private Integer[][] _pesos;
	private int columna;
	private int fila;
	
	public MatrizPesos(int a, int b)
	{
		columna = a;
		fila = b ;
			
		_pesos = new Integer [columna][fila];
	}

	public Integer CantidadDeColumnas()
	{
		return columna;
	}
	
	public Integer CantidadDeFilas()
	{
		return fila;
	}
	
	public void setvalor(Integer a, int columna, int fila)
	{
		this._pesos[columna-1][fila-1] = a;
		this._pesos[fila-1][columna-1] = a;
	}
	
	public Integer getvalor(int columna, int fila)
	{
		return this._pesos[columna-1][fila-1];
	}
	
	
	public void CopiarMatriz(MatrizPesos aux)
	{
		for (int i = 0 ; i < this.columna; i++)
		{
			for (int j = 0 ; i < this.fila; i++)
			{
				this._pesos[i][j] = aux.getvalor(i, j);
			}
		}
	}
	
	public Integer menorPeso(int columna)
	{
		Integer menor = 0;
		for (int i = 1; i < fila-1; i++)
		{
			if (this._pesos[columna][i]!= null)
			{
				if(menor > this._pesos[columna][i])
				{
				menor = this._pesos[columna][i];
				}
			}
		}
		return menor;
	}
	
	public static void main(String[] args) {
		
		MatrizPesos g = new MatrizPesos (4,4);
		
		System.out.println(g.CantidadDeColumnas());
		System.out.println(g.CantidadDeFilas());
		g.setvalor(20, 1, 1);
		g.setvalor(15, 3, 3);
		g.setvalor(10, 2, 3);
		g.setvalor(5, 1, 3);
		System.out.println(g.getvalor(1, 1));
		System.out.println(g.getvalor(2, 1));
	//	System.out.println(g.menorPeso(3));
	}
	
}
