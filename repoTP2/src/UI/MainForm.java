package UI;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import Negocio.MatrizPesos;
import Negocio.agente;
import Negocio.grafo;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class MainForm 
{

	private JFrame frame;
	private JPanel panelMapa;
	private JPanel panelControles;
	private JMapViewer _mapa;
	private ArrayList<Coordinate> _lasCoordenadas;
	private ArrayList< MapPolygonImpl> _losCaminos;
	private ArrayList< agente > _agentes;
	private MatrizPesos _pesos;
	private MatrizPesos _pesosRespaldo;
	private JButton btnEliminar;
	private MapPolygonImpl _poligono;
	private JButton btnPeso;
	private JButton btnEliminarPeso;
	private JButton btndibujarCaminoMinimo ;
	private MapPolygonImpl camino;
	private grafo red;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 725, 506);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panelMapa = new JPanel();
		panelMapa.setBounds(10, 10, 430, 440);
		frame.getContentPane().add(panelMapa);
		
		panelControles = new JPanel();
		panelControles.setBounds(450, 10, 240, 440);
		frame.getContentPane().add(panelControles);
		
		red = new grafo();
		_agentes = new ArrayList<agente>();
		
		panelControles.setLayout(null);
		
		caminoMinimo();
		eliminar();
		dibujarCamino();		
		eliminarCamino();
		
		_mapa = new JMapViewer();
		_mapa.setZoomContolsVisible(true);
		_mapa.setDisplayPositionByLatLon(-34.521671,-58.701238, 16);
		 
		panelMapa.add(_mapa);
		detectarCoordenadas();
	}

	private void eliminarCamino() 
	{
		btnEliminarPeso = new JButton("Eliminar Peso");
		btnEliminarPeso.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				for (int i=0; i<_losCaminos.size();i++)
				{
					_mapa.removeAll();;
				}
				
			}
		});
		btnEliminarPeso.setBounds(10, 168, 195, 23);
		panelControles.add(btnEliminarPeso);
	}

	private void dibujarCamino() 
	{
		btnPeso = new JButton("Peso del camino");
		btnPeso.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				_losCaminos = new ArrayList<MapPolygonImpl>();
				for (int i=0; i<_lasCoordenadas.size()-1; ++i)
				{
					camino = new MapPolygonImpl(_lasCoordenadas.get(i), _lasCoordenadas.get(i+1), _lasCoordenadas.get(i));
					_losCaminos.add(camino);
					_mapa.addMapPolygon(camino);
				}
			}
		});
		btnPeso.setBounds(10, 120, 195, 23);
		panelControles.add(btnPeso);
	}

	private void caminoMinimo() 
	{
		btndibujarCaminoMinimo = new JButton("Dibujar camino");
		btndibujarCaminoMinimo.setBounds(10, 11, 195, 23);
		btndibujarCaminoMinimo.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				_poligono = new MapPolygonImpl(_lasCoordenadas);
				_mapa.addMapPolygon(_poligono);
			}
		});
	}

	private void eliminar() 
	{
		btnEliminar = new JButton("Eliminar camino");
		btnEliminar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				 _mapa.removeMapPolygon(_poligono);
			}
		});
		btnEliminar.setBounds(10, 64, 195, 23);
		panelControles.add(btnEliminar);
		panelControles.add(btndibujarCaminoMinimo);
	}
	
	
	private void detectarCoordenadas() 
	{
		_lasCoordenadas = new ArrayList<Coordinate>();
				
		_mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
			if (e.getButton() == MouseEvent.BUTTON1)
			{
			    Coordinate markeradd = _mapa.getPosition(e.getPoint());
			    _lasCoordenadas.add(markeradd);
				String nombre = JOptionPane.showInputDialog("Nombre: ");
			    _mapa.addMapMarker(new MapMarkerDot(nombre, markeradd));
			    String contacto = JOptionPane.showInputDialog("Ingrese un n�mero de contacto: ");
			    agente nuevo = new agente (nombre,red.vertices(),markeradd,contacto);
		    	_agentes.add(nuevo);
			    if(red.vertices()==0)
			    {	
			    	red.agregarVertice();
			    }
			    else
			    {
			    	int origen = red.vertices();
			    	int destino = Integer.parseInt(JOptionPane.showInputDialog(" ingrese el agente con el cual se encuentra conectado "));
			    	Integer peso = Integer.parseInt(JOptionPane.showInputDialog(" ingrese la distancia entre los agentes "));
			    	red.agregarVertice(destino);
			    	_pesos = new Integer [red.vertices()][red.vertices()];
			    	_pesos.copiar(_auxpesos);
			    	_pesos[origen][destino] = peso;
			    	_pesos[destino][origen] = peso;
			    	_auxpesos = _pesos;
			    }
			    
			}}
		});
	}
}
